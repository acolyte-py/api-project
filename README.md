# api_yamdb
api_yamdb

## Инструкция по загрузке данных из .csv файлов
1. Провести начальную миграцию
> python manage.py migrate
2. Перейти в папку data
3. Запустить скрипт load_data.py
> python load_data.py
